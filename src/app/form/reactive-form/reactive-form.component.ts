import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {

  userForm:FormGroup;
  skillsArray:any=[];

  constructor(
    public formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.setupUserForm();
  }

  setupUserForm(){
    this.userForm = this.formBuilder.group({
      name: this.formBuilder.group({
        first: ['', Validators.compose([Validators.required])],
        last: ['', Validators.compose([Validators.required])]
      }),
      email: ['', Validators.compose([Validators.required, Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')])],
      phone: this.formBuilder.group({
        mobile: ['', Validators.compose([Validators.required])]
      }),
      skills: this.formBuilder.array(['skill 1', 'skill 2'])
    });

    this.skillsArray = this.userForm.get('skills') as FormArray;
    console.log(this.skillsArray);
  }

  removeSkill(idx){
    // console.log(idx);
    this.skillsArray.removeAt(idx);
  }

  onAddSkill(skill){
    // console.log(skill);
    let value = this.formBuilder.control(
      skill
    );
    this.skillsArray.push(value);
  }

}
